import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter, Route } from 'react-router-dom';

import App from './App';
import UsersList from './users/UsersList';
import UserForm from './users/UserForm';
import UserDetails from './users/UserDetails';

const client = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: 'http://localhost:4000/graphql',
  }),
  dataIdFromObject: o => o.id
});


ReactDOM.render(
  <ApolloProvider client={client}>
    <BrowserRouter>
      <div>
        <Route path="/" component={App}/>
        <Route exact path="/users" component={UsersList}/>
        <Route path="/users/:id" component={UserDetails}/>
        <Route path="/new" component={UserForm}/>
      </div>
    </BrowserRouter>
  </ApolloProvider>,
  document.getElementById('root')
);
