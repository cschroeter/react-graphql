import React from 'react';
import { shallow } from 'enzyme';
import { Link } from 'react-router-dom';
import App from './App';


it('renders without crashing', () => {
  shallow(<App/>)
});

it('renders a link to /users', () => {
  const wrapper = shallow(<App />);
  const link = <Link to="/users">Users</Link>;

  expect(wrapper.contains(link)).toEqual(true);
});
