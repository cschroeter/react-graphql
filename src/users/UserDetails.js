import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { fetchUserDetails } from './Queries'
import { Link } from 'react-router-dom';


export class UserDetails extends Component {
  render() {
    const { user } = this.props.data;

    if (!user) {
      return (<div>Loading ...</div>);
    }
    return (
      <div className="container">
        <Link to="/users">Back</Link>
        <h2>UserDetails</h2>
        <dl>
          <dt>Name</dt>
          <dd>{user.firstName}</dd>
          <dt>Age</dt>
          <dd>{user.age}</dd>
        </dl>
      </div>
    )
  }
}

export default graphql(fetchUserDetails, {
  options: (props) => {
    return { variables: { id: props.match.params.id } }
  }
})(UserDetails);
