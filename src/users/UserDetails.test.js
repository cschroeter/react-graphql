import React from 'react';
import { shallow } from 'enzyme';
import { UserDetails }  from './UserDetails';

it('renders without crashing', () => {
  const props = {
    data: {
      loading: false,
      user: null
    }
  };
  shallow(<UserDetails {...props} />)
});

it('renders a loading indicator if no data has been fetched', () => {
  const props = {
    data: {
      loading: true,
      user: {}
    }
  };
  const wrapper = shallow(<UserDetails {...props}/>);

  expect(wrapper).toMatchSnapshot();
});


it('renders a user profile', () => {
  const props = {
    data: {
      loading: false,
      user: {
        firstName: 'Christian',
        age: 30
      }
    }
  };
  const wrapper= shallow(<UserDetails {...props}/>);

  expect(wrapper).toMatchSnapshot();
});
