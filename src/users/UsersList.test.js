import React from 'react';
import { shallow } from 'enzyme';
import { UsersList } from './UsersList';

it('renders without crashing', () => {
  const props = {
    data: {
      loading: false,
      users: []
    }
  };
  shallow(<UsersList {...props}/>)
});

it('renders a loading indicator', () => {
  const props = {
    data: {
      loading: true
    }
  };
  const wrapper = shallow(<UsersList {...props} />);

  expect(wrapper).toMatchSnapshot();
});

it('renders a list of users', () => {
  const props = {
    data: {
      loading: false,
      users: [
        {
          id: 1,
          firstName: 'Christian'
        },
        {
          id: 2,
          firstName: 'Josua'
        }
      ]
    }
  };
  const wrapper = shallow(<UsersList {...props} />);

  expect(wrapper).toMatchSnapshot();
});

