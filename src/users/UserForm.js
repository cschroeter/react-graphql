import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { Link, Redirect } from 'react-router-dom';
import { fetchUsers as query } from './Queries'

export class UserForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      submitted: false
    };
  }

  onSubmit(event) {
    event.preventDefault();

    this.props.mutate({
      variables: {
        firstName: this.state.firstName
      },
      refetchQueries: [{
        query
      }]
    }).then(() => this.setState({submitted: true}));
  }

  render() {
    return (
      <div className="container">
        <Link to="/users">Overview</Link>
        <h2>Create a new user</h2>
        <form onSubmit={(e) => this.onSubmit(e)}>
          <label>User Name</label>
          <input
            onChange={event => this.setState({firstName: event.target.value})}
            value={this.state.firstName}
          />
        </form>
        { this.state.submitted && ( <Redirect to='/users'/>) }
      </div>
    )
  }
}

const mutation = gql`
  mutation AddUser($firstName: String!) {
    addUser(firstName: $firstName) {
      id
      firstName
    }
  }
`;

export default graphql(mutation)(UserForm);

