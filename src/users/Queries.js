import gql from 'graphql-tag';


export const fetchUsers = gql`
  {
    users {
      id
      firstName
    }
  }
`;

export const fetchUserDetails = gql`
  query fetchUserDetails($id: Int!) {
    user(id: $id) {
      id
      firstName
      age
    }
  }
`;

export const deleteUser = gql`
  mutation DeleteUser($id: Int!) {
    deleteUser(id: $id) {id}
  }
`;

