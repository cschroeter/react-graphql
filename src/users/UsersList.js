import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { Link } from 'react-router-dom';
import { fetchUsers, deleteUser } from './Queries'

export class UsersList extends Component {

  onUserDelete(id) {
    this.props
      .mutate({ variables: { id } })
      .then(() => this.props.data.refetch())
  }

  renderUserList() {
    return this.props.data.users.map(({ id, firstName }) => {
      return (
        <li className="collection-item" key={id}>
          <Link to={'/users/'+ id}>{firstName}</Link>
          <a href="#!"><i className="material-icons secondary-content" onClick={() => this.onUserDelete(id)}>delete</i></a>
        </li>
      );
    });
  }

  render() {
    if (this.props.data.loading) {
      return (<div>Loading ...</div>);
    }
    return (
      <div  className="container">
        <ul className="collection">
          {this.renderUserList()}
        </ul>
        <Link to="/new">Create new user</Link>
      </div>

    )
  }
}

export default graphql(deleteUser)(
  graphql(fetchUsers)(UsersList)
);
