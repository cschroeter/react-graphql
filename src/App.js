import React from 'react';
import { Link } from 'react-router-dom';
import './App.css';

export default () => {
  return <nav>
    <div className="nav-wrapper">
      <a href="#" className="brand-logo">GraphQL</a>
      <ul className="right">
        <li><Link to="/users">Users</Link></li>
      </ul>
    </div>
  </nav>
}
